function getXDomainRequest() {
     var xdr = null;

     if (window.XDomainRequest) {
	     xdr = new XDomainRequest(); 
     } else if (window.XMLHttpRequest) {
	     xdr = new XMLHttpRequest(); 
     } else {
	     alert("Check your internet connexion !");
     }

     return xdr;	
}

function register(name, age, sexe) {
     /*var xdr = getXDomainRequest();
     
     xdr.onload = function() {
          //alert(JSON.parse(xdr.responseText));
         var status;
         var data;
         // http://xhr.spec.whatwg.org/#dom-xmlhttprequest-readystate
          status = xdr.status;
          if (status == 200) {
               data = JSON.parse(xdr.responseText);
               //alert('ça marche');
               
          } else {
               console.log('ça marche pas');
          }
          /***
               stocker le token dans le localstorage
          ***/
          /*window.localStorage.setItem('token',data.token);
          
          /***
               aller chercher les contacts du téléphone (si on a le temps)
          ***/
          setTimeout( function () {
          goNext();
          }, 1000);
          
     /*};
     
     xdr.open("POST", "http://10.212.112.173:8000/api/auth/register/", true);
     xdr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
     xdr.send("name=" + name + "&age=" + age + "&sex=" + sexe);*/
}

function addFriend(idfriend) {
     var xdr = getXDomainRequest();
     
     xdr.onload = function() {
          //alert(JSON.parse(xdr.responseText));
         var status;
         var data;
         // http://xhr.spec.whatwg.org/#dom-xmlhttprequest-readystate
           status = xdr.status;
          if (status == 200) {
               //data = JSON.parse(xdr.responseText);
               //alert('ça marche');
               $('#buttonReplace img').remove();
               $('#buttonReplace').append('<button class="button button-icon icon ion-checkmark-round"></button>');
               
               /***
                    créer contact
               ***/
               getAllFriends();
               
          } else {
               $('#buttonReplace img').remove();
               $('#buttonReplace').append('<button class="button button-icon icon ion-close-round"></button>');
               //console.log('ça marche pas');
          }
          setTimeout(function() {
               myPopup.close();
          }, 1000);
          
          
     };
     
     xdr.open("POST", "http://10.212.112.173:8000/api/friends/add/", true);
     xdr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
     xdr.setRequestHeader("Authorization", "Token " + window.localStorage.getItem('token'));
     xdr.send("number=" + idfriend);
}

function getAllFriends() {
     var xdr = getXDomainRequest();
     
     xdr.onload = function() {
          //alert(JSON.parse(xdr.responseText));
         var status;
         var data;
         // http://xhr.spec.whatwg.org/#dom-xmlhttprequest-readystate
           status = xdr.status;
          if (status == 200) {
               data = JSON.parse(xdr.responseText);   
               refreshFriends(data);
          } else {
          
          }
          
          
     };
     
     xdr.open("GET", "http://10.212.112.173:8000/api/friends/list/", true);
     xdr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
     xdr.setRequestHeader("Authorization", "Token " + window.localStorage.getItem('token'));
     xdr.send(null);
     
     
}

function displayFriends(data) {
     var c = 0;
     while (c < data.length) {
          $('#friends ion-list').append('<ion-item href="#/friend/'+data[c].number+'">'+data[c].username+'</ion-item>');
          console.log(data[c].username);
          c++;
     }
}
