var myPopup;

app.controller('PopupCtrl', function ($scope) {
     $scope.execute = function() {
          
          $('#buttonReplace button').remove();
          $('#buttonReplace').append('<img src="img/loading.gif" width="49px"/>');
          /****
               on recherche dans la bdd
          ****/
          
          
          /****
               quand la réponse arrive on met OUI ou NON avec icon
          ****/
          
          
          
          // this is temporary
          setTimeout(function() {
               $('#buttonReplace img').remove();
               $('#buttonReplace').append('<button class="button button-icon icon ion-checkmark-round"></button>');
               setTimeout(function() {
                    myPopup.close();
               }, 1000);
          }, 2000);
          
     };
});

function onBodyClick(e) {
     try {
          if (e.target.outerHTML.split('>')[0] == "<html") {
               myPopup.close();
               $(document).off("click",onBodyClick);
          }
     } catch (err) {
     
     }
};

app.controller('FriendsCtrl', function($scope, Friends, $ionicPopup) {
  $scope.friends = Friends.all();
  
  $scope.showPopupAdd = function() {
          // An elaborate, custom popup
          myPopup = $ionicPopup.show({
               templateUrl: 'addFriend.html',
               scope: $scope
          });
          myPopup.then(function(res) {
               //console.log('Tapped!');
          });
          
          // If an event gets to the body
          setTimeout( function () {
               $(document).on("click", onBodyClick);
          }, 100);
          
     };
});

app.controller('ChatCtrl', function($scope, $stateParams, Friends, User, $ionicScrollDelegate) {
	$scope.user = User.get();
     $scope.friend = Friends.get($stateParams.id);
     $scope.openclose = 0;
     $scope.init = function () {
          setTimeout( function () {
               $ionicScrollDelegate.scrollBottom();
          }, 500);
     };
     
     $('#chat').click(function () {
          $('#chat').css('margin-bottom','0px');
          $('#test').css('height','44px');
          $('#chevron').removeClass('ion-chevron-down');
          $('#chevron').addClass('ion-chevron-up');
          $ionicScrollDelegate.scrollBottom();
     });
     
     $scope.menuOpenClose = function () {
          if ($scope.openclose % 2 == 0) {
               $('#chat').css('margin-bottom','156px');
               $('#test').css('height','200px');
               $('#chevron').removeClass('ion-chevron-up');
               $('#chevron').addClass('ion-chevron-down');
          } else {
               $('#chat').css('margin-bottom','0px');
          $('#test').css('height','44px');
               $('#chevron').removeClass('ion-chevron-down');
               $('#chevron').addClass('ion-chevron-up');
          }
          $scope.openclose++;
          $ionicScrollDelegate.scrollBottom();
     }
     
     $scope.sendMsg = function (urlImg) {
          /****
               envoyer vers la BDD
          ****/
		  // Store the message          
		  // TODO get current localisation
          $scope.friend.messages.push({ id: $scope.user.id, content: urlImg, origin: [48.856929779052734,2.3412001132965088]});
          
          console.log($('#chat .scroll').height());
          
          var test = parseInt($(window).height()) - parseInt($('#test').css('height')) - parseInt($('#header').css('height')) - parseInt($('#chat .scroll').height());
          
          
          
          console.log(test);
          
          if (test < 0) {
               console.log('ca depasse');
               $ionicScrollDelegate.scrollBottom();
          }
     };
          
    $scope.parsePlace = function(locs){
    	return $scope.getPlace(locs);
    }
    $scope.getPlace = function(locs){
    	return "See origin";
    }
    $scope.stripName = function(name){
    	if(name.length > 18){
    		return name.substring(0,17) + "…";
    	}else{
    		return name;
    	}
    };
});

app.controller('MapCtrl', function($scope, $stateParams) {
     $scope.lat = $stateParams.lat;
     $scope.lon = $stateParams.lon;
     $scope.id  = $stateParams.id;
});

app.controller('ConnectCtrl', function($scope, $location, Friends) {
    $scope.goNext = function() {
     $location.path('friends')
    };
});
