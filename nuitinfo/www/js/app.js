// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
var app = angular.module('starter', ['ionic']);

app.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if(window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
    }
    if(window.StatusBar) {
      StatusBar.styleDefault();
    }
  });
})


app.config(function($stateProvider, $urlRouterProvider) {

     $urlRouterProvider.otherwise('/connect');

     $stateProvider.state('connect', {
     url: '/connect',
     templateUrl: 'connect.html',
     controller: 'ConnectCtrl'
     });
     
     $stateProvider.state('friends', {
     url: '/friends',
     templateUrl: 'friends.html',
     controller: 'FriendsCtrl'
     });
     
     $stateProvider.state('friend-chat', {
     url: '/friend/:id',
     templateUrl: 'friend-chat.html',
     controller: 'ChatCtrl'
     });
     
     $stateProvider.state('map', {
     url: '/map/:id/:lat/:lon',
     templateUrl: 'map.html',
     controller: 'MapCtrl'
     });
     

});

