/**
 * A simple example service that returns some data.
 */
app.factory('Friends', function() {
  // Might use a resource here that returns a JSON array

  // Some fake testing data
  // { id, content, origin }
  var friends = [
  {
    "id": 0,
    "name": "Odessa Wagner",
    "messages": [
      {
        "id": 42,
        "content": "smiley-happy.png",
        "origin": [23.909093856811523,-102.63339996337891]
      },
      {
        "id": 0,
        "content": "smiley-happy.png",
        "origin": [48.856929779052734,2.3412001132965088]
      },
      {
        "id": 42,
        "content": "smiley-happy.png",
        "origin": [48.856929779052734,2.3412001132965088]
      },
      {
        "id": 0,
        "content": "smiley-happy.png",
        "origin": [19.431949615478516,-99.133132934570313]
      },
      {
        "id": 0,
        "content": "smiley-happy.png",
        "origin": [-33.874000549316406,151.2030029296875]
      },
      {
        "id": 42,
        "content": "smiley-happy.png",
        "origin": [19.431949615478516,-99.133132934570313]
      }
    ]
  },
  {
    "id": 1,
    "name": "Curry Dalton",
    "messages": [
      {
        "id": 1,
        "content": "smiley-happy.png",
        "origin": [48.856929779052734,2.3412001132965088]
      },
      {
        "id": 1,
        "content": "smiley-happy.png",
        "origin": [48.856929779052734,2.3412001132965088]
      },
      {
        "id": 1,
        "content": "smiley-happy.png",
        "origin": [-33.874000549316406,151.2030029296875]
      },
      {
        "id": 42,
        "content": "smiley-happy.png",
        "origin": [-33.874000549316406,151.2030029296875]
      }
    ]
  },
  {
    "id": 2,
    "name": "Barker Snow",
    "messages": [
      {
        "id": 2,
        "content": "smiley-happy.png",
        "origin": [48.856929779052734,2.3412001132965088]
      },
      {
        "id": 42,
        "content": "smiley-happy.png",
        "origin": [19.431949615478516,-99.133132934570313]
      },
      {
        "id": 2,
        "content": "smiley-happy.png",
        "origin": [19.431949615478516,-99.133132934570313]
      },
      {
        "id": 42,
        "content": "smiley-happy.png",
        "origin": [48.856929779052734,2.3412001132965088]
      },
      {
        "id": 2,
        "content": "smiley-happy.png",
        "origin": [19.431949615478516,-99.133132934570313]
      },
      {
        "id": 42,
        "content": "smiley-happy.png",
        "origin": [-33.874000549316406,151.2030029296875]
      },
      {
        "id": 2,
        "content": "smiley-happy.png",
        "origin": [48.856929779052734,2.3412001132965088]
      },
      {
        "id": 42,
        "content": "smiley-happy.png",
        "origin": [48.856929779052734,2.3412001132965088]
      }
    ]
  },
  {
    "id": 3,
    "name": "Pitts Baldwin",
    "messages": [
      {
        "id": 3,
        "content": "smiley-happy.png",
        "origin": [-33.874000549316406,151.2030029296875]
      },
      {
        "id": 42,
        "content": "smiley-happy.png",
        "origin": [-33.874000549316406,151.2030029296875]
      },
      {
        "id": 42,
        "content": "smiley-happy.png",
        "origin": [48.856929779052734,2.3412001132965088]
      },
      {
        "id": 42,
        "content": "smiley-happy.png",
        "origin": [48.856929779052734,2.3412001132965088]
      },
      {
        "id": 3,
        "content": "smiley-happy.png",
        "origin": [-33.874000549316406,151.2030029296875]
      },
      {
        "id": 42,
        "content": "smiley-happy.png",
        "origin": [19.431949615478516,-99.133132934570313]
      },
      {
        "id": 42,
        "content": "smiley-happy.png",
        "origin": [19.431949615478516,-99.133132934570313]
      },
      {
        "id": 42,
        "content": "smiley-happy.png",
        "origin": [19.431949615478516,-99.133132934570313]
      }
    ]
  },
  {
    "id": 4,
    "name": "Clara Sanchez",
    "messages": [
      {
        "id": 4,
        "content": "smiley-happy.png",
        "origin": [48.856929779052734,2.3412001132965088]
      },
      {
        "id": 4,
        "content": "smiley-happy.png",
        "origin": [19.431949615478516,-99.133132934570313]
      },
      {
        "id": 4,
        "content": "smiley-happy.png",
        "origin": [-33.874000549316406,151.2030029296875]
      },
      {
        "id": 4,
        "content": "smiley-happy.png",
        "origin": [48.856929779052734,2.3412001132965088]
      },
      {
        "id": 4,
        "content": "smiley-happy.png",
        "origin": [-33.874000549316406,151.2030029296875]
      },
      {
        "id": 4,
        "content": "smiley-happy.png",
        "origin": [-33.874000549316406,151.2030029296875]
      }
    ]
  },
  {
    "id": 5,
    "name": "Bishop Stone",
    "messages": [
      {
        "id": 5,
        "content": "smiley-happy.png",
        "origin": [19.431949615478516,-99.133132934570313]
      },
      {
        "id": 42,
        "content": "smiley-happy.png",
        "origin": [48.856929779052734,2.3412001132965088]
      },
      {
        "id": 5,
        "content": "smiley-happy.png",
        "origin": [19.431949615478516,-99.133132934570313]
      },
      {
        "id": 5,
        "content": "smiley-happy.png",
        "origin": [19.431949615478516,-99.133132934570313]
      },
      {
        "id": 42,
        "content": "smiley-happy.png",
        "origin": [-33.874000549316406,151.2030029296875]
      },
      {
        "id": 5,
        "content": "smiley-smiley-happy.png",
        "origin": [19.431949615478516,-99.133132934570313]
      }
    ]
  }
];
  return {
    all: function() {
      return friends;
    },
    get: function(friendId) {
      // Simple index lookup
      return friends[friendId];
    },
  }
});
app.factory('User', function(){
  var user = { id: 42, name: 'Super Dupont', messages: []};
  return {
    get: function(){ return user;}
  }
})