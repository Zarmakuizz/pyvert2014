Glimlag
=====================

Glimlag is a project that aims to make the communcation easier between peoples of different languages and cultures. Glimlag means Smile in Afrikans, a language based on a melting pot of many other languages, thus symbolizing a meeting accross multiple cultures.

Because of the culture and language difference, a single prononciation might target two complete different meaning between two different languages. This, and the gestures are also cultural-related. 

What is left as a universal way of communication? Pictures! A smiling face will always communicate a happy feeling. Our app then enables to communicate through symbols. These messages quickly communicates where you are and what's going on: are you safe, are you in danger, is an army coming near your place, etc. A simple yet efficient way to communicate between NPO and users in danger.

This project is used based on Ionic, a framework build on :

* Cordova, projet to build crossplatform web apps

* AngularJS, Javascript framework

## Using Ionic

We recommend using the [Ionic CLI](https://github.com/driftyco/ionic-cli) to create new Ionic projects that are based on this project but use a ready-made starter template.

More info on this can be found on the Ionic [Getting Started](http://ionicframework.com/getting-started) page and the [Ionic CLI](https://github.com/driftyco/ionic-cli) repo.

You'll need to install Node.JS, Cordova and Ionic in order to convert this project info mobile apps. Please follow the [Ionic Installation instructions](http://ionicframework.com/docs/guide/installation.html).
